import express from 'express';
import socket from 'socket.io';
import { MessageModel } from '../models';
import { json } from 'body-parser';

class MessageController {
    io: socket.Server;

    constructor(io: socket.Server) {
        this.io = io;
    }

    index = (req: express.Request, res: express.Response) => {
        const dialogId: any = req.query.dialog;

        MessageModel.find({ dialog: dialogId })
            .populate(["dialog", "author"])
            .exec(function (err:any, messages:any) {
                if (err) {
                    return res.status(404).json({
                        message: "not messages"
                    });
                }
                if (messages) {
                    return res.json(messages);
                }
            })
    }

    indexAll = (req: express.Request, res: express.Response) => {
        const projectId: any = req.params.projectId;

        console.log(projectId)

        MessageModel.find({ project: projectId })
            .exec(function (err:any, messages:any) {
                if (err) {
                    return res.status(404).json({
                        message: "not messages"
                    });
                }
                if (messages) {
                    return res.json(messages);
                }
            })
    }

    delete = (req: express.Request, res: express.Response) => {
        const id: string = req.params.id;
        MessageModel.findOneAndDelete({ _id: id })
            .then((message:any) => {
                if (!message) {
                    return res.status(404).json({
                        message: `message ${id} not found`
                    })
                }
                res.json({
                    message: `Succsess ${message.id} deleted`
                });
            }).catch((err:any) => {
                return res.status(404).json(err)
            });
    }

    create = (req: express.Request, res: express.Response) => {

        let user = <any>{};
        user = req.user;

        const postData = {
            text: req.body.text,
            author: req.body.author,
            project: req.body.projectId,
        }
        const message = new MessageModel(postData);

        message
            .save()
            .then((obj: any) => {
                res.json(obj);
                this.io.emit('SERVER:NEW_MESSAGE', obj);
            }).catch(reason => {
                return res.status(500).json({
                    message: reason
                });
            })
    }

}

export default MessageController;