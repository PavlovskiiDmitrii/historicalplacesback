import express from "express";
import socket from "socket.io";
import { TourModel } from "../models";

class TourController {

  all = (req: express.Request, res: express.Response) => {
    TourModel.find((err:any, tour:any) => {
      if (err) {
          return res.status(404).json({
              message: "not found Tour"
          })
      }
      if (tour) {
          return res.json(tour);
      }
  })
  };

  create = (req: express.Request, res: express.Response) => {
    const postData = {
      name: req.body.name,
      price: req.body.price,
      members: req.body.members,
    };

    const tour = new TourModel(postData);
    tour
      .save()
      .then((obj: any) => {
        res.json({
          message: obj,
        });
      })
      .catch((reason) => {
        res.status(500).json({
          status: "error",
          message: "234234",
        });
      });
  };
}

export default TourController;
