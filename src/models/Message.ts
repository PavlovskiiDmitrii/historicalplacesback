import mongoose, { Schema, Document } from 'mongoose';
import { DialogModel, UserModel, MonumentModel } from '../models';


export interface IMessage extends Document {
    text: {
        type: string;
        require: boolean;
    },
    author: {
        type: Schema.Types.ObjectId;
        ref: string;
        require: true;
    },
    dialog: {
        type: Schema.Types.ObjectId;
        ref: string;
        require: true;
    },
    project: {
        type: Schema.Types.ObjectId;
        ref: string;
        require: true;
    },
    unread: {
        type: boolean;
        default: boolean;
    }
}

const MessageSchema = new Schema({ 
    text: {type: String, require: Boolean, required: true},
    author: {type: Schema.Types.ObjectId, ref: UserModel, required: true},
    unread: {
        type: Boolean,
        default: false
    },
    dialog: {type: Schema.Types.ObjectId, ref: DialogModel},
    project: {type: Schema.Types.ObjectId, ref: MonumentModel}
}, {
    timestamps: true
});

const MessageModel = mongoose.model<IMessage >('message', MessageSchema);

export default MessageModel;