import express from 'express';
import dotenv from 'dotenv';
var cors = require('cors');
const bodyParser = require("body-parser")

import { createServer } from 'http';

import './core/db';
import { createRoures } from "./core";
import createSocket from "./core/socket";

const app = express();

app.use(cors({origin: 'http://localhost:3000'}));
app.use(bodyParser.urlencoded({ extended: true }));

const http = createServer(app);
const io = createSocket(http);

dotenv.config();

createRoures(app, io);

http.listen(process.env.PORT , () => {
  console.log(`Server: http://localhost:${process.env.PORT}`);
});