import express from 'express';
import { updateLastSeen, checkAuth } from '../middleware';
import boduParser from 'body-parser';
import socket from 'socket.io';

import {
  UserCtrl,
  DialogCtrl,
  MessageCtrl,
  TaskCtrl,
  MonumentCtrl,
  TourCtrl
} from "../controller"

const createRoures = (app: express.Express, io: socket.Server) => {

  const UserController = new UserCtrl(io);
  const TourController = new TourCtrl();
  // const DialogController = new DialogCtrl(io);
  // const MessageController = new MessageCtrl(io);
  // const TaskController = new TaskCtrl(io);
  const MonumentController = new MonumentCtrl(io);

  app.use(boduParser.json());
  app.use(updateLastSeen);

  app.post('/user/create', UserController.create);
  app.post('/user/login', UserController.login);
  app.get('/user/me/:id', UserController.getMe);
  app.get('/user/:id', UserController.index);
  
  app.post('/monument/create', MonumentController.create);
  app.post('/monument/addcomment/:id', MonumentController.addcomment);
  app.get('/monument/comments/:id', MonumentController.comments);
  app.get('/monument/:id', MonumentController.index);
  app.get('/monuments', MonumentController.all);
  
  app.post('/tour/create', TourController.create);
  app.get('/tour/all', TourController.all);


  // app.get('/users', UserController.getAll);
  // app.delete('/user/:id', UserController.delete);

  // app.post('/project/create', MonumentController.create);
  // app.post('/project/adduser/:id', MonumentController.adduser);
  // app.post('/project/removeuser/:id', MonumentController.removeuser);
  // app.post('/project/retitle/:id', MonumentController.retitle);
  // app.post('/project/redescription/:id', MonumentController.redescription);
  // app.post('/project/changperformed/:id', MonumentController.changperformed);
  // app.delete('/project/:id', MonumentController.delete);
  // app.get('/project/:id', MonumentController.index);
  // app.get('/projects', MonumentController.all);

  // app.get('/task/:id', TaskController.index);
  // app.get('/tasks/:projectId', TaskController.indexAll);
  // app.post('/task/create', TaskController.create);
  // app.post('/task/adduser/:id', TaskController.adduser);
  // app.post('/task/removeuser/:id', TaskController.removeuser);
  // app.post('/task/retitle/:id', TaskController.retitle);
  // app.post('/task/redescription/:id', TaskController.redescription);
  // app.post('/task/changperformed/:id', TaskController.changperformed);
  // app.post('/task/setprogress/:id', TaskController.setprogress);
  // app.delete('/task/:id', TaskController.delete);

  // app.get('/dialogs/:projectId', DialogController.index);
  // app.post('/dialog/create', DialogController.create);
  // app.delete('/dialog/:id', DialogController.delete);

  // app.get('/messages', MessageController.index);
  // app.get('/messages/:projectId', MessageController.indexAll);
  // app.post('/messages', MessageController.create);
  // app.delete('/message/:id', MessageController.delete);
}

export default createRoures;