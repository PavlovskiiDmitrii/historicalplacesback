import express from 'express';
import socket from 'socket.io';
import { TaskModel } from '../models';


class TaskController {
    io: socket.Server;

    constructor(io: socket.Server) {
        this.io = io;
    }

    index(req: express.Request, res: express.Response) {
        const id: string = req.params.id;
        TaskModel.findById(id, (err:any, task:any) => {
            if (err) {
                return res.status(404).json({
                    message: "not Tack"
                })
            }
            if (task) {
                res.json(task);
            }
        })
    }


    indexAll(req: express.Request, res: express.Response) {
        
        const projectId: any = req.params.projectId;

        TaskModel.find({ project: projectId }).
        exec(function (err:any, tasks:any) {
            if (err) {
                return res.status(404).json({
                    message: "not task"
                });
            }
            if (tasks) {
                return res.json(tasks);
            }
        })
    }

    adduser(req: express.Request, res: express.Response) {
        const id: string = req.params.id;
        TaskModel.findByIdAndUpdate(id, {
            $push: {
                partners: req.body.partners
            }
        },
            { new: true }, function (err, task) {
                if (err) {
                    return res.status(404).json({
                        message: "not found Tack"
                    })
                }
                if (task) {
                    res.json(task);
                }
            })
    }

    removeuser(req: express.Request, res: express.Response) {
        const id: string = req.params.id;
        TaskModel.findByIdAndUpdate(id, {
            $pull: {
                partners: req.body.partners
            }
        },
            { new: true }, function (err, task) {
                if (err) {
                    return res.status(404).json({
                        message: "not found Partners"
                    })
                }
                if (task) {
                    res.json(task);
                }
            })
    }

    retitle(req: express.Request, res: express.Response) {
        const id: string = req.params.id;
        TaskModel.findByIdAndUpdate(id, {
            title: req.body.title
        },
            { new: true }, function (err, task) {
                if (err) {
                    return res.status(404).json({
                        message: "not found Tack"
                    })
                }
                if (task) {
                    res.json(task);
                }
            })
    }

    redescription(req: express.Request, res: express.Response) {
        const id: string = req.params.id;
        TaskModel.findByIdAndUpdate(id, {
            description: req.body.description
        },
            { new: true }, function (err, task) {
                if (err) {
                    return res.status(404).json({
                        message: "not found Tack"
                    })
                }
                if (task) {
                    res.json(task);
                }
            })
    }

    changperformed(req: express.Request, res: express.Response) {
        const id: string = req.params.id;
        let lastValuePerformed;

        TaskModel.findById(id)
            .then((task:any) => {
                lastValuePerformed = task?.performed ? false : true;
                console.log(lastValuePerformed);
                // как обновить не смотря еще раз в базу!!!!
                TaskModel.findByIdAndUpdate(id, {
                    performed: task
                },
                    { new: true }, function (err:any, task:any) {
                        if (err) {
                            return res.status(404).json({
                                message: "not found Tack"
                            })
                        }
                        if (task) {
                            res.json(task);
                        }
                    })

            })
    }

    setprogress(req: express.Request, res: express.Response) {
        const id: string = req.params.id;

        TaskModel.findByIdAndUpdate(id, {
            progress: req.body.progress
        },
        { new: true }, function (err, task) {
            if (err) {
                return res.status(404).json({
                    message: "not found Tack"
                })
            }
            if (task) {
                res.json(task);
            }
        })
    }

    delete(req: express.Request, res: express.Response) {
        const id: string = req.params.id;
        TaskModel.findOneAndDelete({ _id: id })
            .then((task:any) => {
                if (!task) {
                    return res.status(404).json({
                        message: `task ${id} not found`
                    })
                }
                res.json({
                    message: `Succsess ${task.title} deleted`
                });
            }).catch((err:any) => {
                return res.status(404).json(err)
            });
    }

    create(req: express.Request, res: express.Response) {
        const authorId = "5e592077d6314832fd5793e3";
        const defaultProgress = 0;
        const defaultPerformed = false;

        console.log(req)

        const postData = {
            title: req.body.title,
            description: req.body.description,
            author: authorId,
            partners: req.body.partners,
            progress: defaultProgress,
            performed: defaultPerformed,
            project: req.body.projectId,
        }
        const task = new TaskModel(postData);

        task
            .save()
            .then((taskObj: any) => {
                res.json({
                    message: "Create new Task",
                    status: 'success'
                });
            }).catch(reason => {
                res.json(reason);
            })
    }

}

export default TaskController;