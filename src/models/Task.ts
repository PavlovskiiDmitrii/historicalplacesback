import mongoose, { Schema, Document } from 'mongoose';
import { UserModel, MonumentModel } from '../models';


export interface ITask extends Document {
    title: {
        type: string;
    },
    description: {
        type: string;
    },
    author: {
        type: Schema.Types.ObjectId;
        ref: string;
        require: true;
    },
    partners: {
        type: Schema.Types.ObjectId;
        ref: string;
        require: true;
    },
    progress: {
        type: Number;
        ref: string;
        min: 0;
        max: 100;
        require: true;
    },
    performed: {
        type: boolean;
        default: boolean;
        require: true;
    },
    project : {
        type: Schema.Types.ObjectId;
        ref: string;
        require: true;
    }
}

const TaskSchema = new Schema({ 
    title: {type: String, require: Boolean, required: true},
    description: {type: String, require: Boolean, required: true},
    author: {type: Schema.Types.ObjectId, ref: UserModel, required: true},
    partners: [{type: Schema.Types.ObjectId, ref: UserModel}],
    progress: {type: String, required: true},
    performed: { type: Boolean, default: false },
    project: {type: Schema.Types.ObjectId, ref: MonumentModel}
}, {
    timestamps: true
});

const TaskModel = mongoose.model<ITask >('task', TaskSchema);

export default TaskModel;