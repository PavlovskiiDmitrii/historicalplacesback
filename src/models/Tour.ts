import mongoose, { Schema, Document } from 'mongoose';


export interface ITour extends Document {
    name: string,
    price: string,
    members: Schema.Types.Array,
}

const TourSchema = new Schema({ 
    name: {
        type: String
    },
    price: {
        type: String
    },
    members:  Schema.Types.Array
});

const TourModel = mongoose.model<ITour>('tour', TourSchema);

export default TourModel;