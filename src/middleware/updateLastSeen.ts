import { UserModel } from '../models'
import express from 'express';

export default (_: express.Request, __: express.Response, next: express.NextFunction) => {
    UserModel.findOneAndUpdate(
        {_id: '5e592077d6314832fd5793e3'},
        {
            fullname: "qwe",
            last_seen: new Date()
        },
        {new: true}, () => {}
    );
    next();
}