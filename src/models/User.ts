import mongoose, { Schema, Document } from 'mongoose';
import isEmail from 'validator/lib/isEmail';


export interface IUser extends Document {
    email: string,
    name: string,
    password: string,
    comfermed: boolean,
    foto?: string,
    role?: string,
    last_seen?: Date;
}

const UserSchema = new Schema({ 
    email: {
        type: String,
        validate: [isEmail, 'Invalid email'],
        unique: true
    },
    name: {
        type: String,
        required: "Name is required"
    },
    password: {
        type: String,
        required: "Password is required"
    },
    comfermed: {
        type: Boolean,
        default: false
    },
    foto: String,
    role: String,
    last_seen: {
        type: Date,
        default: new Date()
    }
}, {
    timestamps: true
});

const UserModel = mongoose.model<IUser>('user', UserSchema);

export default UserModel;