import express from 'express';
import socket from 'socket.io';
import { DialogModel, MessageModel } from '../models';


class DialogController {
    io: socket.Server;
    
    constructor (io: socket.Server) {
        this.io = io;
    }

    index = (req: express.Request, res: express.Response) => {

        const projectId: any = req.params.projectId;

        DialogModel.find({ project: projectId })
            .populate(["author", "partners"])
            .exec(function (err:any, dialogs:any) {
                if (err) {
                    return res.status(404).json({
                        message: "not dialogs"
                    });
                }
                if (dialogs) {
                    return res.json(dialogs);
                }
            })
    }

    delete = (req: express.Request, res: express.Response) => {
        const id: string = req.params.id;
        DialogModel.findOneAndDelete({ _id: id })
            .then((dialog:any) => {
                if (!dialog) {
                    return res.status(404).json({
                        message: `dialog ${id} not found`
                    })
                }
                res.json({
                    message: `Succsess ${dialog.name} deleted`
                });
            }).catch((err:any) => {
                return res.status(404).json(err)
            });
    }

    create = (req: express.Request, res: express.Response) => {
        const authorId = "5e592077d6314832fd5793e3";

        const postData = {
            author: authorId,
            partners: req.body.partners,
            name: req.body.name
        }
        const dialog = new DialogModel(postData);

        dialog
            .save()
            .then((dialogObj: any) => {
                const message = new MessageModel({
                    text: req.body.text,
                    dialog: dialogObj._id,
                    author: req.body.author
                });

                message.save()
                    .then((messageObj: any) => {
                        res.json({
                            message: "Create new Dialog"
                        })
                    })
                    .catch(reason => {
                        res.json(reason);
                    })

            }).catch(reason => {
                res.json(reason);
            })
    }

}

export default DialogController;