import jwt from 'jsonwebtoken';

export default (token: string) =>
  new Promise((resolve, reject) => {
    console.log(token)
    jwt.verify(token, process.env.JWT_SECRET || '', (err, decodedData) => {
      if (err || !decodedData) {
        console.log(err)
        return reject(err);
      }
      resolve(decodedData);
    });
  });