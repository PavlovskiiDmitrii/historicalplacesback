const Influx = require('../../node_modules/influx')

export default () => {

    const influx = new Influx.InfluxDB({
        host: 'localhost',
        database: 'tasks_visits',
    })
      
    influx.getDatabaseNames()
        .then((names : any) => {
            if (!names.includes('tasks_visits')) {
                return influx.createDatabase('tasks_visits');
            }
        })
        .catch((err : any) => {
            console.error(`Error creating Influx database!`);
    });

    const writeTaskTime = (TaskId: number, userId: number, dateVisitOpen: FormData, dateVisitClose: FormData) => {
        influx.writePoints([
            {
                measurement: 'tasks_visits_time',
                tags: {
                    task_id: TaskId,
                    user_id: userId,
                },
                fields: { open: dateVisitOpen, close: dateVisitClose }
            }
        ], {
            database: 'tasks_visits'
        })
    }
};