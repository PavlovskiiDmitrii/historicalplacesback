import express from 'express';
import socket from 'socket.io';
import { MonumentModel, TaskModel, DialogModel, MessageModel } from '../models';


class MonumentController {
    io: socket.Server;

    constructor(io: socket.Server) {
        this.io = io;
    }

    // adduser(req: express.Request, res: express.Response) {
    //     const id: string = req.params.id;
    //     MonumentModel.findByIdAndUpdate(id, {
    //         $push:{
    //           partners: req.body.partners  
    //         }
    //     },
    //     {new: true}, function (err, task) {
    //         if (err) {
    //             return res.status(404).json({
    //                 message: "not found Project"
    //             })
    //         }
    //         if (task) {
    //             res.json(task);
    //         }
    //     })
    // }

    // removeuser(req: express.Request, res: express.Response) {
    //     const id: string = req.params.id;
    //     MonumentModel.findByIdAndUpdate(id, {
    //         $pull: {
    //             partners: req.body.partners
    //         }
    //     },
    //         { new: true }, function (err, task) {
    //             if (err) {
    //                 return res.status(404).json({
    //                     message: "not found Partners"
    //                 })
    //             }
    //             if (task) {
    //                 res.json(task);
    //             }
    //         })
    // }

    // retitle(req: express.Request, res: express.Response) {
    //     const id: string = req.params.id;
    //     MonumentModel.findByIdAndUpdate(id, {
    //         title: req.body.title
    //     },
    //         { new: true }, function (err, project) {
    //             if (err) {
    //                 return res.status(404).json({
    //                     message: "not found Prject"
    //                 })
    //             }
    //             if (project) {
    //                 res.json(project);
    //             }
    //         })
    // }

    // redescription(req: express.Request, res: express.Response) {
    //     const id: string = req.params.id;
    //     MonumentModel.findByIdAndUpdate(id, {
    //         description: req.body.description
    //     },
    //         { new: true }, function (err, project) {
    //             if (err) {
    //                 return res.status(404).json({
    //                     message: "not found Project"
    //                 })
    //             }
    //             if (project) {
    //                 res.json(project);
    //             }
    //         })
    // }

    // changperformed(req: express.Request, res: express.Response) {
    //     const id: string = req.params.id;
    //     let lastValuePerformed;

    //     MonumentModel.findById(id)
    //         .then((project:any) => {
    //             lastValuePerformed = project?.performed ? false : true;
    //             console.log(lastValuePerformed);
    //             // как обновить не смотря еще раз в базу!!!!
    //             MonumentModel.findByIdAndUpdate(id, {
    //                 performed: project
    //             },
    //                 { new: true }, function (err:any, project:any) {
    //                     if (err) {
    //                         return res.status(404).json({
    //                             message: "not found Project"
    //                         })
    //                     }
    //                     if (project) {
    //                         res.json(project);
    //                     }
    //                 })

    //         })
    // }

    // delete(req: express.Request, res: express.Response) {
    //     const id: any = req.params.id;
    //     MonumentModel.findOneAndDelete({ _id: id })
    //         .then((project:any) => {
    //             if (!project) {
    //                 return res.status(404).json({
    //                     message: `project ${id} not found`
    //                 })
    //             }
    //             res.json({
    //                 message: `Succsess ${project.title} deleted`
    //             });
    //         }).catch((err:any) => {
    //             return res.status(404).json(err)
    //         });
    //     DialogModel.findOneAndDelete({ project: id })
    //         .exec(function (err:any, dialog:any) {
    //             if (err) {
    //                 console.log("not dialog")
    //             }
    //             if (dialog) {
    //                 console.log(`dialog ${dialog._id} remove`);            
    //                 MessageModel.deleteMany({ dialog: dialog._id })
    //                     .exec(function (err:any, messages:any){
    //                         if(err) {
    //                             console.log(err);
    //                         }
    //                         if(messages) {
    //                             console.log('Messages removed')
    //                         }
    //                     }) 
    //             }
    //         })
    //     TaskModel.deleteMany({ project: id })
    //         .exec(function (err:any, tasks:any) {
    //             if(err) {
    //                 console.log(err);
    //             }
    //             if(tasks) {
    //                 console.log('Tasks removed')
    //             }
    //         })
    // }

    create(req: express.Request, res: express.Response) {
        // const authorId = "5e592077d6314832fd5793e3";
        // const defaultProgress = 0;
        // const defaultPerformed = false;

        const postData = {
            title: req.body.title,
            name: req.body.name,
            disc: req.body.disc,
            photo: req.body.photo,
            geometry: req.body.geometry,
            comments: req.body.comments,
        }
        const monument = new MonumentModel(postData);

        monument.save()
            .then((monumentObj: any) => {
                // const FirstTask = new TaskModel({
                //     title: 'Main Tast',
                //     description: 'Descroption on Main task',
                //     author: authorId,
                //     partners: authorId,
                //     progress: defaultProgress,
                //     performed: defaultPerformed,
                //     project: projectObj._id
                // })
                // FirstTask.save().then((obj: any) => {
                //     console.log("Create new Task")                    
                // })

                // const Dialog = new DialogModel({
                //     author: authorId,
                //     partners: req.body.partners,
                //     name: req.body.title + ' dialog',
                //     project: projectObj._id
                // })
                // Dialog.save().then((dialogObj: any) => {
                //     console.log("Create new DIalog")     
                // })
                res.json({
                    status: 'Sucsses',
                    message: "Create new Monument"
                });
            }).catch(reason => {
                res.json(reason);
            })
    }

    addcomment(req: express.Request, res: express.Response) {
        const id: string = req.params.id;
        MonumentModel.findByIdAndUpdate(id, {
            $push:{
               comments: req.body.comments  
            }
        },
        {new: true}, function (err, task) {
            if (err) {
                return res.status(404).json({
                    message: "not found Project"
                })
            }
            if (task) {
                res.json(task);
            }
        })
    }

    index(req: express.Request, res: express.Response) {
        const id: any = req.params.id;
        MonumentModel.findById(id, (err:any, project:any) => {
            if (err) {
                return res.status(404).json({
                    message: "not MonumentModel"
                })
            }
            if (project) {
                return res.json(project);
            }
        })
    }

    comments(req: express.Request, res: express.Response) {
        const id: any = req.params.id;
        MonumentModel.findById(id, (err:any, project:any) => {
            if (err) {
                return res.status(404).json({
                    message: "not MonumentModel"
                })
            }
            if (project) {
                return res.json(project.comments);
            }
        })
    }

    all(req: express.Request, res: express.Response) {
        MonumentModel.find((err:any, monuments:any) => {
            if (err) {
                return res.status(404).json({
                    message: "not found MonumentModels"
                })
            }
            if (monuments) {
                return res.json(monuments);
            }
        })
    }

}

export default MonumentController;