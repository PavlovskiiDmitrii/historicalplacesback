import express from "express";
import socket from "socket.io";
import { UserModel } from "../models";
import { createJWTToken } from "../utils";
import { IUser } from "../models/User";

class UserController {
  io: socket.Server;

  constructor(io: socket.Server) {
    this.io = io;
  }

  index = (req: express.Request, res: express.Response) => {
    const id: string = req.params.id;
    UserModel.findById(id, (err: any, user: any) => {
      if (err) {
        return res.status(404).json({
          message: "not User1",
        });
      }
      if (user) {
        res.json(user);
      }
    });
  };

  getMe = (req: express.Request, res: express.Response) => {
    const id: string = req.params.id;
    UserModel.findById(id, (err: any, user: any) => {
      if (err) {
        return res.status(404).json({
          message: "not User2",
        });
      }
      if (user) {
        res.json(user);
      }
    });
  };

  create = (req: express.Request, res: express.Response) => {
    const postData = {
      email: req.body.email,
      name: req.body.name,
      password: req.body.password,
      role: req.body.role,
    };

    const user = new UserModel(postData);
    user
      .save()
      .then((obj: any) => {
        res.json({
          message: obj,
        });
      })
      .catch((reason) => {
        res.status(500).json({
          status: "error",
          message: "1111111111",
        });
      });
  };

  login = (req: express.Request, res: express.Response) => {
    const postData = {
      email: req.body.email,
      password: req.body.password,
    };

    UserModel.findOne({ email: postData.email }, (err: any, user: IUser) => {
      if (err) {
        return res.status(404).json({
          message: "user not found",
        });
      }

      try {
        if (user.password === postData.password) {
          const token = createJWTToken(user);
          res.json({
            status: "success",
            token,
            user,
          });
        } else {
          res.json({
            status: "error",
            message: "not pass or email",
          });
        }
      } catch {
        res.json({
          status: "error",
          message: "not pass or email",
        });
      }
    });
  };

  // getAll = (req: express.Request, res: express.Response) => {
  //     UserModel.find((err : any, user : any) => {
  //         if (err) {
  //             return res.status(404).json({
  //                 message: "not User2"
  //             })
  //         }
  //         if (user) {
  //             res.json(user);
  //         }
  //     })
  // }

  // delete = (req: express.Request, res: express.Response) => {
  //     const id: string = req.params.id;
  //     UserModel.findOneAndDelete({ _id: id })
  //         .then((user:any) => {
  //             if (!user) {
  //                 return res.status(404).json({
  //                     message: `user ${id} not found`
  //                 })
  //             }
  //             res.json({
  //                 message: `Succsess ${user.name} deleted`
  //             });
  //         }).catch((err:any) => {
  //             return res.status(404).json(err)
  //         });
  // }
}

export default UserController;
