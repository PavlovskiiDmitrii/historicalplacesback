const neo4j = require('neo4j-driver')
import Neode from 'neode';

export default (express: any, app : any) => {
    const driver = neo4j.driver('bolt://localhost:7687', neo4j.auth.basic("neo4j", "123456"));
    const session = driver.session();

    const AddActivitis= (user : any, activiti : any) => {
        session
        .run(`CREATE(n:User {_id:"${user._id}"}) 
                CREATE(a:Activiti{title:"${activiti.title}"}) 
                CREATE (n)-[r:ACTIVITI]->(a)`)
        .then((result : any) => {
            result.records.forEach((record : any) => {
                console.log(record._fields[0].properties);
            })
            session.close();
        })
        .catch((err : any) => {
            console.log("ERRR - " + err)
        })
    }

    const GetAllActivitis = () => {
        session
        .run("MATCH(n:Project) RETURN n")
        .then( (result : any) => {
            result.records.forEach((record : any) => {
                console.log(record._fields[0].properties);
            })
        }).catch((err : any) => {
            console.log("ERRR - " + err)
        })
    }
    AddActivitis({_id : '12d231d'}, {title : 'Create Projekt'});
};