import mongoose, { Schema, Document } from 'mongoose';
import { UserModel, MonumentModel } from '../models';
import isEmail from 'validator/lib/isEmail';


export interface IDialog extends Document {
    author: {
        type: Schema.Types.ObjectId;
        ref: string;
    },
    partners: {
        type: Schema.Types.ObjectId;
        ref: string;
        require: true;
    },
    name: String,
    project : {
        type: Schema.Types.ObjectId;
        require: true;
    };
}

const DialogSchema = new Schema({ 
    author: {type: Schema.Types.ObjectId, ref: UserModel},
    partners: [{type: Schema.Types.ObjectId, ref: UserModel}],
    name: String,
    project: {type: Schema.Types.ObjectId, ref: String}
}, {
    timestamps: true
});

const DialogModel = mongoose.model<IDialog >('dialog', DialogSchema);

export default DialogModel;