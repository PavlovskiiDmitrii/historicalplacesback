import mongoose, { Schema, Document } from 'mongoose';
import { UserModel } from '../models';

export interface IMonument extends Document {
    title: String,
    name: String,
    photo: Schema.Types.Array,
    disc: String,
    geometry: Schema.Types.Array,
    comments: Schema.Types.Array
}

const MonumentSchema = new Schema({ 
    title: String,
    name: String,
    photo: Schema.Types.Array,
    disc: String,
    geometry: Schema.Types.Array,
    comments: Schema.Types.Array
});

const MonumentModel = mongoose.model<IMonument >('Monument', MonumentSchema);

export default MonumentModel;